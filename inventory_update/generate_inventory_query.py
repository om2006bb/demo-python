import csv


month_last_date_map = {
    9: 30,
    10: 31,
    11: 30,
    12: 31,
    1: 31,
    2: 30,
    3: 30,
}


def generate_query(room_ids = None):
    f = open('inv_room.csv', 'r')
    writer = open('inv_query.sql', 'w')
    reader = csv.reader(f)
    skipped_header = False
    for row in reader:
        if skipped_header == True:
            if room_ids is None or int(row[2]) in room_ids:
                start_date_str, end_date_str, room_id, price = row[0], row[1], row[2], row[3]
                if not start_date_str or not end_date_str or not room_id or not price:
                    print ('Error: Data missing {}'.format(str(row)))
                    continue
                writer.write("UPDATE inventory_roominventory SET price={} WHERE date BETWEEN '{}' and '{}' "
                    "and room_id={};\n".format(price, start_date_str, end_date_str, room_id))
        else:
            skipped_header = True


def get_property_inv_queries(row):
    queries = []
    start_date_str, end_date_str, room_id, price = row[0], row[1], row[2], row[3]
    start_year, start_month, start_date = [int(ele) for ele in start_date_str.split('-')]
    end_year, end_month, end_date = [int(ele) for ele in end_date_str.split('-')]

    year = start_year
    while year <= end_year:
        month = start_month
        while month <= end_month:
            date = start_date
            while date <= month_last_date_map[month]:
                #queries.append("UPDATE inventory_roominventory SET price={} WHERE date='{}' and room_id={}".format(price, '{}-{}-{}'.format(start_year, month, date), room_id))
                queries.append("INSERT into inventory_roominventory(created_at, updated_at, date, available_units, sold_units, price, room_id) VALUES(now(), now(), '{}', 1, 0, {},{})".format('{}-{}-{}'.format(year, month, date),price, room_id))
                date += 1
            month += 1
        year += 1
    return queries


def generate_insert_query(room_ids = None):
    f = open('inv_b.csv', 'rU')
    writer = open('inv_insert_query.sql', 'w')
    reader = csv.reader(f)
    skipped_header = False
    for row in reader:
        if skipped_header == True:
            if room_ids is None or int(row[2]) in room_ids:
                start_date_str, end_date_str, room_id, price = row[0], row[1], row[2], row[3]
                if not start_date_str or not end_date_str or not room_id or not price:
                    print ('Error: Data missing {}'.format(str(row)))
                    continue
                for query in get_property_inv_queries(row):
                    writer.write(query + ";\n")
        else:
            skipped_header = True

generate_insert_query()
