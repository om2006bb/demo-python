import json
import requests
import urllib2
import csv
import sys
import codecs
koden=sys.stdin.encoding
import MySQLdb
import csv
import datetime
from time import gmtime, strftime

db = MySQLdb.connect(host="localhost",  # your host, usually localhost
                     user="root",  # your username
                     passwd="",  # your password
                     db="altacco_backend")  # name of the data base


def prepare_hotel_dict():
    dict = {}
    fp = open('hotels_data.csv', 'r')
    rows = csv.reader(fp)
    for row in rows:
        dict[row[2].lower()] = row
    return dict

def prepare_roomtype_dict():
    f = open('test_roomtype.csv', 'rb')
    reader = csv.reader(f)
    dict = {}
    for row in reader:
        if dict.get(row[1].lower()):
            dict[row[1].lower()] = row[0]
            dict[row[1].lower().replace(' ', '')] = row[0]
        if not dict.get(row[1].lower().replace(' ','')) and not dict.get(row[1].lower()):
            dict[row[1].lower().replace(' ', '')] = row[0]
            dict[row[1].lower()] = row[0]
    return dict

def get_propertyimage_data(db):

    hotel_dict = prepare_hotel_dict()
    roomtype_dict = prepare_roomtype_dict()

    fp = open('mmt_image.csv', 'w')
    fields = ['hotel_seq_id',
              'image_Name',
              'Image_title',
              'Image_Url',
              'Active_Status',
              'image_type',
              'level_code', #roomtype RMTPSEQ
              'IS_LISTING_IMG',
              'created_date',
              'updated_by',
              'height',
              'width']

    writer = csv.writer(fp)
    writer.writerow(fields)

    # you must create a Cursor object. It will let
    #  you execute all the queries you need
    cur = db.cursor()
    import hashlib

    room_dict = prepare_roomtype_dict()
    props, prop_result, prop_dict = [], [], {}
    f = open('hotel_mapping.csv', 'rb')
    reader = csv.reader(f)
    for row in reader:
        if row[0] == 'HOT_HTLSEQ':
            continue
        prop_dict[int(row[0])] = [row[1], row[2], row[3]]
        prop_result.append(row[0])
    f.close()
    print len(prop_result)

    # Use all the SQL you like
    cur.execute("Select PI.*, P.name from properties_propertyimage PI, properties_property P WHERE PI.property_id = P.id and PI.is_disabled=0"
                " and PI.property_id in "+str(tuple(prop_result)))

    # print all the first cell of all the rows
    rows = cur.fetchall()

    for row in rows:

        hotel_id = row[-3:][0]
        print hotel_id
        #hotel_seq_id = hotel_dict[hotel_name][2]
        hotel_seq_id = prop_dict[hotel_id][0]
        Image_Url = row[2]
        image_hash = hashlib.md5(Image_Url).hexdigest()
        cur.execute(
            "select cloudinary_base_url, height, width from images_sourceimage where tag = 'PROPERTY' and "
            "ref_url_hash = '"+image_hash+"'")
        source_image_results = cur.fetchall()
        if not source_image_results:
            continue
        img_url, height, width = source_image_results[0][0], source_image_results[0][1], source_image_results[0][2]
        image_Name = Image_Url.split('/')[-1:][0]
        Active_Status = 'A'

        IS_LISTING_IMG = 1 if row[5] == 1 else 0

        writer.writerow(["'"+hotel_seq_id+"'", image_Name, row[1], img_url, Active_Status, 'H', 0, IS_LISTING_IMG,
                         strftime('%Y-%m-%d %H:%M:%S', gmtime(int(row[3]))), 'auto', height, width])

    #writer
    # Use all the SQL you like
    cur.execute("Select RI.*, P.name, R.name as room_name, R.property_id from properties_propertyroomimage RI, properties_propertyroom R, properties_property P WHERE R.property_id = P.id and RI.room_id = R.id and RI.is_disabled=0"
                " and R.property_id in "+str(tuple(prop_result)))

    # print all the first cell of all the rows
    rows = cur.fetchall()
    # proproom_dict =  {}
    # f = open('rooms_data.csv', 'rb')
    # reader = csv.reader(f)
    # for row in reader:
    #     if row[0] == 'HRF_HTLSEQ':
    #         continue
    #     proproom_dict[int(row[0])] = row[1]
    # f.close()

    for row in rows:
        room_name = row[10].lower()
        hotel_name = row[9].lower()
        #hotel_seq_id = hotel_dict[hotel_name][2]
        print hotel_name
        hotel_seq_id = prop_dict[int(row[-1])][0]
        Image_Url = row[2]
        image_Name = Image_Url.split('/')[-1:][0]
        image_hash = hashlib.md5(Image_Url).hexdigest()
        cur.execute(
            "select cloudinary_base_url, height, width from images_sourceimage where tag = 'ROOM' and "
            "ref_url_hash = '" + image_hash + "'")
        source_image_results = cur.fetchall()
        if not source_image_results:
            continue
        img_url, height, width = source_image_results[0][0], source_image_results[0][1], source_image_results[0][2]
        Active_Status = 'A'
        IS_LISTING_IMG = 0
        room_type, room_type_seq, room_dict = get_room_type(roomtype_dict, room_name)
        if room_type_seq == '':
            print row[1]
            continue
        level_code = room_type_seq
        # level_code = proproom _dict[int(row[11])]
        # level_code = roomtype_dict[room_name][0] if room_name in roomtype_dict else None

        writer.writerow(["'"+hotel_seq_id+"'", image_Name, row[1], img_url, Active_Status, 'R', level_code, IS_LISTING_IMG,
                         strftime('%Y-%m-%d %H:%M:%S', gmtime(int(row[3]))), 'auto', height, width])

    list = rows
    for row in list[:5]:
        print row

    fp.close()
    return rows

def get_room_type(room_dict, name):
    k=1
    if room_dict.get(name.lower().replace(' ','')):
        if room_dict.get(name.lower().replace(' ',''))=='1':
            return name, '', room_dict
        # print name, name.lower().replace(' ',''), room_dict.get(name.lower().replace(' ',''))
        return name, room_dict.get(name.lower().replace(' ','')), room_dict
    else:
        # if name.split(' ')[0] in ['1','2','3','4','5','8']:
        #     if room_dict.get(name.lower().split(' ')[0]+ name.lower().split(' ')[1]):
        #         # print name, name.lower().split(' ')[0]+name.lower().split(' ')[1], room_dict.get(name.lower().split(' ')[0]+ name.lower().split(' ')[1])
        #         return name, room_dict.get(name.lower().split(' ')[0]+ name.lower().split(' ')[1]), room_dict
        # else:
        #     if room_dict.get(name.lower().split(' ')[0]):
        #         if 'bhk' in name.lower() and  len(name.lower().split(' '))>1 and 'bhk' not in (name.lower().split(' ')[0]+name.lower().split(' ')[1]):
        #             pass
        #         else:
        #             if 'bhk' in name.lower().split(' ')[0] and len(name.lower().split(' '))>1 and 'bungalow' not in name.lower():
        #                 if room_dict.get(name.lower().split(' ')[0]+name.lower().split(' ')[1]):
        #                     # print name, name.lower().split(' ')[0]+name.lower().split(' ')[1], room_dict.get(name.lower().split(' ')[0]+name.lower().split(' ')[1])
        #                     return name, room_dict.get(name.lower().split(' ')[0]+name.lower().split(' ')[1]), room_dict
        #             else:
        #                 # print name, name.lower().split(' ')[0], room_dict.get(name.lower().split(' ')[0])
        #                 return name, room_dict.get(name.lower().split(' ')[0]), room_dict
        # i, y = 1, 1
        # for key in room_dict.keys():
        #     i += 1
        #     if key == name.lower().replace('_', ' '):
        #         y, k = 0, key
        #         break
        #     if key == name.lower().replace('_', ' '):
        #         y, k = 0, key
        #     x = name.split(' ')
        #     if x[0].lower() in key:
        #         y,k=0,key
        #     if key in name.lower():
        #         y,k=0,key
        #     if (i == len(room_dict) and y == 0):
        #         return name, room_dict.get(k), room_dict
        # if y==0:
        #     return name, room_dict.get(k), room_dict
        res= [ '', name,'', 'A', 'By Rightstay', strftime('%Y-%m-%d %H:%M:%S', gmtime()),'','','']
        # res = [property_id,room_id,name]
        fp = open('roomtype_new.csv', 'a')
        writer = csv.writer(fp)
        # writer.writerow(roomtype_columns)
        writer.writerow(res)
        room_dict[name.lower().replace(' ','')] = '1'
    return name,'',room_dict

def prepare_facility_data():
    dict = {}
    fp = open('test_facility.csv', 'r')
    rows = csv.reader(fp)
    for row in rows:
        dict[row[4].lower()] = row
    return dict



def get_mmt_facility_data(db):
    facility_mapping = {}
    fp = open('facility_new2.csv', 'w')

    writer = csv.writer(fp)
    writer.writerow(['FCL_NAME',
                     'FCL_DESC',
                     'FCL_ACTSTAT',
                     'FCL_FCLTTP',
                     'FACILITY_GROUP_ID',
                     'FCL_FCLTCD'])
    cur = db.cursor()
    cur.execute("Select master_type, display_name, name, description, is_enabled, id FROM base_mastertype WHERE master_type in ('PropertyAmenity', 'PropertyRoomAmenity', 'PropertyBathroomAmenity') and is_enabled>0")

    rows = cur.fetchall()
    facility_dict = prepare_facility_data()
    y=1

    for row in rows:
        i,y,l = 1,1,1
        if row[2]=='24_hr_checkin':
            y=0
            facility_mapping[row[2].lower()] = '24 hour checkin'+'_'+'58933'
        if row[2] == '24_hr_security':
            y = 0
            facility_mapping[row[2].lower()] = '24 hour security' + '_' + '1781'
        if row[2] == 'shared_tv_lounge':
            y = 0
            facility_mapping[row[2].lower()] = 'TV Lounge' + '_' + '1575'
        if row[2] == 'ledlcd_tv':
            y = 0
            facility_mapping[row[2].lower()] = 'Television' + '_' + '307'
        if row[2] == 'heater':
            y = 0
            facility_mapping[row[2].lower()] = 'Room Heater' + '_' + '118'
        if row[2] == 'halllounge_area':
            y = 0
            facility_mapping[row[2].lower()] = 'Lounge' + '_' + '2197'
        if row[2] == 'shower_panel':
            y = 0
            facility_mapping[row[2].lower()] = 'Shower' + '_' + '59238'
        if row[2] == '24*7_power_back_up':
            y = 0
            facility_mapping[row[2].lower()] = '24 hours power back-up' + '_' + '795'
        if row[2] == 'breakfast_included':
            y = 0
        if row[2] == 'lunch_included':
            y = 0
        if row[2] == 'dinner_included':
            y = 0
        if y!=0:
            for key in facility_dict.keys():
                i+=1
                if key == row[2].lower().replace('_', ' '):
                    y, k, l = 0, key,0
                    print row, facility_dict[k][4]
                    facility_mapping[row[2].lower()]=facility_dict[k][4]+'_'+facility_dict[k][0]
                    break
                if key==row[2].lower().replace('_',' ') or row[1].lower() in key:
                    if row[2].lower()=='heater':
                        facility_mapping[row[2].lower()] = facility_dict[k][4]+'_'+facility_dict[key][0]
                        break
                    y,k=0,key
                if (i==len(facility_dict) and y==0) :
                    facility_mapping[row[2].lower()] = facility_dict[k][4]+'_'+facility_dict[k][0]
                    print row,facility_dict[k][4]

        if y:
            type = 'HOTEL'
            FACILITY_GROUP_ID = 54
            if row[0] == 'PropertyRoomAmenity':
                FACILITY_GROUP_ID = 20
            elif row[0] == 'PropertyBathroomAmenity':
                FACILITY_GROUP_ID = 40
            FCL_FCLTCD = row[5]
            Active_Status = 'A'
            if FACILITY_GROUP_ID in (20,40):
                type='ROOM'

            writer.writerow([row[1], '', Active_Status, type, FACILITY_GROUP_ID, ''])

    list = rows
    # for row in list[:5]:
    #     print row

    mapping_list = [[key,val] for key,val in facility_mapping.items()]
    fp.close()
    fp = open('facility_mapping_dict.csv', 'w')
    writer = csv.writer(fp)
    for mapping  in mapping_list:
        writer.writerow(mapping)
    fp.close()

    return rows

def get_htl_rmtype_data(db):
    roomtype_dict = prepare_roomtype_dict()
    fp = open('mmt_htl_rm_type.csv', 'w')
    fields = [
     'HRT_RMTPNAME',
     'HRT_HTLSEQ',
     'HRT_CITYCD',
     'HRT_RMTPSEQ',
     'HRT_HTLNAME_backup',
     'HRT_CRUSRID',
     'HRT_CRDT',
     'HRT_LASTUPDUSRID',
     'HRT_LASTUPDDT',
     'HRT_HTLNAME',
     'HRT_STATUS']
    writer = csv.writer(fp)
    writer.writerow(fields)
    cur = db.cursor()
    props, prop_result, prop_dict = [], [], {}
    roomname_dict = preapare_roomname_dict()
    f = open('hotel_mapping.csv', 'rb')
    reader = csv.reader(f)
    for row in reader:
        if row[0] == 'HOT_HTLSEQ':
            continue
        prop_dict[int(row[0])] = [row[1], row[2], row[3]]
        prop_result.append(row[0])
    f.close()
    print len(prop_result)

    cur.execute("Select P.name, R.name, R.property_id as room_name from properties_propertyroom R, properties_property P WHERE R.property_id = P.id "
                " and R.property_id in "+str(tuple(prop_result)))
    rows = cur.fetchall()
    for row in rows:
        room_name = row[1].lower()
        hotel_name = row[0]
        hotel_id = row[2]
        #hotel_seq_id = hotel_dict[hotel_name][2]
        print hotel_name
        # hotel_seq_id = hotel_dict[hotel_name][0] if hotel_name in hotel_dict else None
        # city_cd = hotel_dict[hotel_name][1] if hotel_name in hotel_dict else None
        city_cd = prop_dict[int(hotel_id)][1]
        hotel_name = prop_dict[int(hotel_id)][2]
        hotel_seq_id = prop_dict[int(hotel_id)][0]
        Active_Status = 'A'
        room_type, room_type_seq, room_dict = get_room_type(roomtype_dict, room_name)
        if room_type_seq == '':
            print row[1]
            continue
        level_code = room_type_seq
        room_name = roomname_dict.get(room_type_seq)
        if room_name is None:
            continue
        # level_code = roomtype_dict[room_name][0] if room_name in roomtype_dict else None
        writer.writerow([room_name, "'"+hotel_seq_id+"'", city_cd, level_code, hotel_name, 'By Rightstay', strftime('%Y-%m-%d %H:%M:%S', gmtime()), '', '', hotel_name, Active_Status
                         ])
    list = rows
    fp.close()
    return rows

def preapare_roomname_dict():
    f = open('test_roomtype.csv', 'rb')
    reader = csv.reader(f)
    dict = {}
    for row in reader:
        dict[row[0]] = row[1]
    return dict

def create_meal_type():
    fp = open('meal_code_new.csv', 'w')
    fields = ['MLP_ACTSTAT   MLP_CD   MLP_NAME   MLP_DESC   MLP_SRV_TAX   MLP_CRUSRID   MLP_CRDT   MLP_LASTUPDUSRID   MLP_LASTUPDDT   MLP_SRVTAXPERCENT']
    new_fields = ['LUNCH','DINNER','2 Meals-Lunch & Dinner']
    writer = csv.writer(fp)
    writer.writerow(fields)
    for row in new_fields:
        writer.writerow(['A','','',row,'','','','','',''])
    fp.close()

def create_automation_csv(db):
    props, prop_result, prop_dict = [], [], {}
    f = open('hotel_mapping.csv', 'rb')
    reader = csv.reader(f)
    for row in reader:
        if row[0] == 'HOT_HTLSEQ':
            continue
        prop_dict[int(row[0])] = [row[1], row[2], row[3], row[5]]
        prop_result.append(row[0])
    f.close()
    # create_meal_type()
    def get_city_by_lat_lng(lat, lng):
        res = requests.get('http://services.mygola.com/ds/getCityAtLatLng?lat={}&lng={}'.format(lat, lng))
        res = json.loads(res.content)
        return res['data']['name']

    def get_meal_code_by_list(list):
        if list is None or len(list) == 0:
            return 'EP'

        if len(list) == 3:
            return 'AP'

        if len(list) == 2:
            if 202 not in list:
                return 'LD'
            if 474 not in list:
                return 'SMAP'
            if 475 not in list:
                return 'MAP'

        if 202 in list:
            return 'CP'
        if 474 in list:
            return 'LN'
        if 475 in list:
            return 'DN'


    property_meals_dict = {}
    cur = db.cursor()
    cur.execute("Select property_id, GROUP_CONCAT(mastertype_id  order by mastertype_id SEPARATOR ',' ) meal_list from properties_property_amenities WHERE mastertype_id in (202, 474, 475) "
                "and property_id in (372,302,106,423,2592) GROUP by property_id")
    rows = cur.fetchall()
    for row in rows:
        property_meals_dict[row[0]] = row[1]

    fp = open('automation.csv', 'w')
    fields = ['country',
     'city',
     'hotel',
     'RoomType',
     'Market Type',
     'Tariff Type',
     'Description',
     'Minimum No of Nights',
     'Max No of Nights',
     'Valid From',
     'Valid To',
     'Meal Plan',
     'Inclusion',
     'Service Tax %',
     'Service Tax Payable By',
     'Settlement Mode',
     'Rate Type',
     'Commission Percentage',
     'Cancellation Template',
     'Base Adult Accomodation',
     'Tariff for 1st adult',
     'ExtraAdult',
     'Allow More Adults',
     'Extra adult price',
     'Allow Children',
     'Max no. of Children Allowed\xc3\xa6*',
     'No. of Children Without Extra Bed',
     'No. of Children With Extra Bed',
     'From Age',
     'To Age',
     'With/Without extra bed',
     'Tariff for child w/o extra bed',
     'Inventory per day for Total Validity Period',
     'Release Inventory',
     'No Of Rooms to be Released',
     'Tax Included in Tariff']

    writer = csv.writer(fp)
    writer.writerow(fields)
    room_dict = prepare_roomtype_dict()
    roomname_dict = preapare_roomname_dict()
    cur = db.cursor()
    cur.execute("Select P.name, R.name as room_name, P.description, P.min_nights, P.max_nights, "
                "P.service_tax, PC.mastertype_id, R.adults, R.max_extra_beds, R.extra_bed_price, "
                "R.price, R.host_discount, P.platform_discount, R.total_rooms, P.latitude, P.longitude, P.id,"
                "R.property_id "
                "from properties_propertyroom R, properties_property P, properties_property_cancellation_policies PC "
                "WHERE R.property_id = P.id and PC.property_id = P.id"
                " and R.property_id in (372,302,106,423,2592)")
    rows = cur.fetchall()
    for row in rows:
        room_name = row[1]
        hotel_name = row[0]
        hotel_desc = row[2]
        min_nights = row[3]
        max_nights = row[4]
        if not max_nights:
            max_nights = 3
        print hotel_name, room_name
        commission_percent = 0 # settings.COMISSION
        service_tax = row[5]
        service_tax = 1.5

        cancellation_policies = {
            223: 'Relaxed: 24 hours, No Refund',
            224: 'Optimal: 3 days, No Refund',
            225: 'Strict: 7 days, No Refund',
            226: 'Super Strict: 15 days, No Refund',
            537: 'Non Refundable after booking',
        }.get(row[6], '')
        adults = row[7]
        price = row[10]
        host_discount = row[11]
        platform_discount = row[12]
        lat, lng = row[14], row[15]
        new_dict = {}
        states = ['andhra pradesh', 'arunachal pradesh', 'assam', 'bihar', 'chhattisgarh', 'gujarat', 'haryana',
                  'himachal pradesh', 'jharkhand', 'karnataka', 'kerala', 'madhya pradesh', 'maharashtra', 'manipur',
                  'meghalaya', 'mizoram', 'nagaland', 'odisha', 'punjab', 'rajasthan', 'sikkim', 'tamilnadu',
                  'telangana', 'tripura', 'uttar pradesh', 'uttarakhand', 'west bengal']


        room_type, room_type_seq, room_dict = get_room_type(room_dict, row[1])
        room_type, room_type_seq, room_dict = get_room_type(room_dict, row[1])
        if room_type_seq == '':
            print row[1]
            continue
        room_name = roomname_dict.get(room_type_seq)
        property_id = row[16]
        meal_type = get_meal_code_by_list(property_meals_dict.get(property_id))

        adult_price = int(price * (1 - host_discount * 0.01) * (1 - platform_discount * 0.01))
        adults_tariff = ', '.join([str(adult_price)] * adults)
        max_extra_beds = row[8]
        extra_bed_price = int(int(row[9]) * (1 - host_discount * 0.01) * (1 - platform_discount * 0.01))
        VALID_TO = '12/31/2017 0:00'
        MAX_CHILD_ALLOWED = 2
        max_extra_beds = 0
        allow_more_adults = 'Y' if max_extra_beds > 0 else 'N'
        release_inventory = -1
        inventory_per_day_for_total_validity_period = row[13]
        tax_included_in_tariff = 'Yes' if service_tax <= 0 else 'No'
        city_name = prop_dict[int(row[17])][3]
        writer.writerow(['India', city_name, hotel_name.split('- By Rightstay')[0]+'- By Rightstay', room_name, 'Both', 'Standalone', 'Rightstay', min_nights,
                         max_nights, strftime('%m/%d/%Y %H:%M', gmtime()), VALID_TO, meal_type, '',
                         service_tax, 'Customer',
                         'Cut off pay', 'Commissionable Rate', commission_percent, cancellation_policies,
                         adults, adults_tariff, 0, allow_more_adults, extra_bed_price,
                         'NO', 0, 0, 0, 0, 5, 'No', 0,
                         inventory_per_day_for_total_validity_period, release_inventory, 'All', tax_included_in_tariff])
    list = rows
    for row in list[:5]:
        print row
    fp.close()
    return rows

def prepare_city_dict():
    f = open('city.csv', 'rb')
    reader = csv.reader(f)
    dict = {}
    for row in reader:
        dict[row[-8].lower()] = row[0]
        x = row[-8].lower().split(' ')
        for y in x:
            dict[y] = row[0]
    return dict

def get_citycode(dict, name):
    rss = name.lower().replace(' ','')
    if dict.get(name.lower()):
        return dict.get(name.lower())
    return 'p'

def prepare_hotel_mapping(db):
    f = open('test_hotels.csv', 'rb')
    k = open('hotel_mapping.csv','w')
    cur = db.cursor()
    reader = csv.reader(f)
    for row in reader:
        if row[0] == 'HOT_HTLSEQ' or row[2] == '':
            continue
        print row[2]
        name = str(row[2].split('- By')[0]).replace('//','').replace('\\','').split("'")[0]
        cur.execute("select id from properties_property where name like '"+name+"%'")
        prop_id = cur.fetchall()
        writer = csv.writer(k)
        if prop_id:
            lis= [int(prop_id[0][0])]
            lis.extend([row[0],row[1],row[:-6][-1],row[2],row[29]])
            writer.writerow(lis)
    f.close()


def get_facility_htl_mapping_data(db):
    fp = open('mmt_facility_htl_mapping.csv', 'w')

    writer = csv.writer(fp)
    # writer.writerow(['hotel_seq_id', 'FCL_FCLTCD'])

    facility_dict, mapping_dict = prepare_facilitynew_data()

    # you must create a Cursor object. It will let
    #  you execute all the queries you need
    cur = db.cursor()

    # Use all the SQL you like

    fetch_data = "p.id,p.name,city_id,description,creation_ts,lastModified,status,emails,contacts,website,address," \
                 "contactperson,pincode,p.latitude,p.longitude,listing_type,id_image_url,landmark,checkin_time_display_id," \
                 "checkout_time_display_id,region_id,type_id,is_deleted,address_line_2,area,short_description,priority," \
                 "platform_discount,alternate_contact,r.name"
    result = []
    props, prop_result, prop_dict = [], [], {}
    f = open('hotel_mapping.csv', 'rb')
    reader = csv.reader(f)
    for row in reader:
        if row[0] == 'HOT_HTLSEQ':
            continue
        prop_dict[int(row[0])] = [row[1], row[2],row[3]]
        prop_result.append(row[0])
    f.close()
    print len(prop_result)

    # cur.execute("select * from (Select property_id, mastertype_id from properties_property_amenities "
    #             "UNION "
    #             "(Select PR.property_id, PRA.mastertype_id from properties_propertyroom PR, "
    #                 "(  Select * from properties_propertyroom_amenities "
    #                     "UNION "
    #                     "Select * from properties_propertyroom_bathroom_amenities) PRA "
    #                     "WHERE PR.id = PRA.propertyroom_id"
    #                 ")) P"
    #             " where P.property_id in "+ str(tuple(prop_result)))

    cur.execute(
        "select property_id,(select name from base_mastertype where id = mastertype_id) from properties_property_amenities"
        " where property_id in "+str(tuple(prop_result)))

    # print all the first cell of all the rows
    rows = cur.fetchall()
    writer.writerow(['HFC_HTLSEQ', 'HFC_CITYCD', 'HFC_FCLTCD', 'HFC_HTLNAME', 'HFC_ICON', 'HFC_CRUSRID', 'HFC_CRDT',
                     'HFC_LASTUPDUSRID', 'HFCLASTUPDDT', 'DELETED', 'PRIORITY'])
    for row in rows:
        facility_cd = get_facility_code(facility_dict, mapping_dict, row[1])
        if facility_cd=='':
            print row[1]
            continue
        writer.writerow(["'"+prop_dict[int(row[0])][0]+"'",prop_dict[int(row[0])][1],facility_cd,prop_dict[int(row[0])][2],'',
                         'By Rightstay',strftime('%Y-%m-%d %H:%M:%S', gmtime()),'','',0,''])

    fp.close()

    list = rows
    return rows

def prepare_facilitynew_data():
    dict,fdict= {},{}
    # fp = open('facility_dump.csv', 'r')
    # rows = csv.reader(fp)
    # for row in rows:
    #     dict[row[4].lower()] = row
    # fp.close()
    fp = open('facility_mapping_dict.csv', 'r')
    rows = csv.reader(fp)
    for row in rows:
        fdict[row[0]] = row[1]
    fp.close()
    return dict,fdict

def get_facility_code(facility_dict, mapping_dict, name):
    val = mapping_dict.get(name.lower())
    if val:
        val = val.split('_')[-1]
    else:
        val = ''
    return val


# get_propertyimage_data(db)
#get_mmt_facility_data(db)
#get_facility_htl_mapping_data(db)





