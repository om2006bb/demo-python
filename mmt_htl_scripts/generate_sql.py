import json
import urllib2
import csv
import sys
import codecs
koden=sys.stdin.encoding
import MySQLdb
import csv
import requests
from time import gmtime, strftime

db_obj = MySQLdb.connect(host="localhost",  # your host, usually localhost
                     user="root",  # your username
                     passwd="root",  # your password
                     db="altacco_backend")  # name of the data base

def generate_properties_mmtidmapping_sql(db = db_obj, filename = 'mmt_htl_rm_type.csv'):
    IS_ALL_PROPERTY = True

    mmt_htl_name_id_rooms_dict = {}
    with open(filename, 'r') as csv_f:
        rows = csv.DictReader(csv_f)
        for row in rows:
            right_property_name = row['HRT_HTLNAME'].replace('- By Rightstay', '').lower()
            mmt_room_name_id_dict = mmt_htl_name_id_rooms_dict.get(right_property_name)
            if not mmt_room_name_id_dict:
                mmt_room_name_id_dict = {}
                mmt_htl_name_id_rooms_dict[right_property_name] = [row['HRT_HTLSEQ'], mmt_room_name_id_dict]
            else:
                mmt_room_name_id_dict = mmt_room_name_id_dict[1]
            mmt_room_name_id_dict[row['HRT_RMTPNAME'].lower()] = row['HRT_RMTPSEQ']

    r_mmtidmapping = {}
    cur = db.cursor()
    cur.execute("SELECT P.id, P.name as property_name, R.id, R.name as room_name FROM properties_propertyroom R, properties_property P WHERE R.property_id = P.id {}".format(
                "and R.property_id=112" if not IS_ALL_PROPERTY else ''))
    rows = cur.fetchall()
    for row in rows:
        r_property_id = row[0]
        r_property_name = row[1].lower()
        r_room_id = row[2]
        r_room_name = row[3].lower()

        if not mmt_htl_name_id_rooms_dict.get(r_property_name):
            print('Error| Rightstay property not found | name = {}'.format(r_property_name))
            continue

        mmt_htl_id, mmt_rooms = mmt_htl_name_id_rooms_dict[r_property_name]

        r_mmt_room_mapping =  r_mmtidmapping.get(mmt_htl_id)
        if not r_mmt_room_mapping:
            r_mmt_room_mapping = {}
            r_mmtidmapping[mmt_htl_id] = [r_property_id, r_mmt_room_mapping]
        else:
            r_mmt_room_mapping = r_mmt_room_mapping[1]

        if not mmt_rooms.get(r_room_name):
            print('Error| Rightstay property "{}" room "{}" not found'.format(r_property_name, r_room_name))
            continue
        r_mmt_room_mapping[mmt_rooms[r_room_name]] = r_room_id

    sql_list = []
    for mmt_htl_id, room_val in r_mmtidmapping.items():
        r_htl_id, room_val = room_val
        sql_list.append('insert into properties_mmtidmapping(type, mmt_id, rightstay_id) values({}, "{}", {})'.format(
            2, mmt_htl_id, r_htl_id
        ))
        for mmt_roomtype_id, r_room_id in room_val.items():
            sql_list.append('insert into properties_mmtidmapping(type, mmt_id, rightstay_id) values({}, "{}", {})'.format(
                1, mmt_roomtype_id, r_room_id
            ))

    out_f_name = 'properties_mmtidmapping.sql'
    with open(out_f_name, 'w') as out_f:
        for sql in sql_list:
            out_f.write(sql + ';\n')



generate_properties_mmtidmapping_sql()