
import requests
from hashlib import sha512
import json

txnid = 'ALTWEBTEST1849'
PAYU_SALT = '52J5ilXK'
PAYU_MERCHANT_KEY = 'myhSfS'
PAYU_VERIFY_URL = 'https://test.payu.in/merchant/postservice.php?form=2'
key = PAYU_MERCHANT_KEY
command = 'verify_payment'
var1 = txnid
hash = sha512('{}|{}|{}|{}'.format(key, command, var1, PAYU_SALT)).hexdigest()
headers = {
    "Content-Type": "application/x-www-form-urlencoded"
}
data = {
    'key': key,
    'command': command,
    'hash': hash,
    'var1': var1
}
response = requests.post(PAYU_VERIFY_URL, data=data, headers=headers)
result = json.loads(response.text)

print result

request_status = result.get('status')
transaction_details = result.get('transaction_details')
if request_status == 1 and transaction_details and txnid in transaction_details:
    status = transaction_details[txnid].get('status')
    if status == 'success':
        print status