import csv
import pyexcel as pe
from pyexcel.ext import xlsx

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

def generate_insert_query1():
    data = pe.get_book_dict(file_name="Cities Data.xlsx")
    print (data)
    data = data.get("Sheet1")
    print (len(data))
    writer = open('prop_update_query_1.sql', 'w')
    for row in data[1:]:
        id = int(row[0])
        display_name = str(row[14])
        if not id or not display_name:
            print ('Error: Data missing {}'.format(str(row)))
            continue
        query = 'UPDATE properties_property SET display_name="{}" WHERE id={}'.format(display_name, id)
        writer.write(query + ";\n")


def generate_insert_query2():
    data = pe.get_book_dict(file_name="Display_names.xlsx")
    print (data)
    data = data.get("Sheet1")
    print (len(data))
    writer = open('prop_update_query_2.sql', 'w')
    for row in data[1:]:
        id = int(row[0])
        display_name = str(row[3])
        if not id or not display_name:
            print ('Error: Data missing {}'.format(str(row)))
            continue
        query = 'UPDATE properties_property SET display_name="{}" WHERE id={}'.format(display_name, id)
        writer.write(query + ";\n")

def generate_insert_query3():
    data = pe.get_book_dict(file_name="Regions Oct 14 455 PM.xlsx")
    data = data.get("Sheet1")
    writer = open('prop_update_query_4.sql', 'w')
    for row in data[1:]:
        id = int(row[0])
        display_name = str(row[15])
        if not id or not display_name:
            print ('Error: Data missing {}'.format(str(row)))
            continue
        query = 'UPDATE properties_property SET display_name="{}" WHERE id={}'.format(display_name, id)
        writer.write(query + ";\n")


# generate_insert_query1()
# generate_insert_query2()
generate_insert_query3()