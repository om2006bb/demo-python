from xml.etree.ElementTree import fromstring, iterparse
from StringIO import StringIO

def get_rate_id(xml_response, room_code, date):
    xml_rooms = xml_response[1]
    for xml_room in xml_rooms:
        if str(xml_room.attrib['Code']) == str(room_code):
            xml_data = xml_room[0]
            if xml_data.attrib['Date'] == date:
                return xml_data[0][0].attrib['Id']

def get_data():
    xml_str = ''
    xml_object = fromstring(xml_str)
    xml_response = xml_object[1][0][0][0]
    print get_rate_id(xml_response, room_code=7546, date='2016-07-26')

def get_data():
    xml = """<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Header>
      <ResponseId xmlns="http://www.makemytrip.com/hotels/">1a7cdce8-5f01-4a60-889d-fd2ae017188e@172-16-105-1</ResponseId>
   </soap:Header>
   <soap:Body>
      <ns4:FetchHotelAllotmentResponse xmlns:ns4="http://www.makemytrip.com/hotels/response/" xmlns:ns2="http://www.makemytrip.com/hotels/" xmlns:ns3="http://www.makemytrip.com/hotels/request/">
         <ns4:FetchHotelAllotmentResult>
            <ns4:response status="Success">
               <ns4:Hotel Code="112" Name="AAA Hotel and Restaurant" />
               <ns4:Rooms>
                  <ns4:Room Code="7546" CurrencyCode="INR" Name="super deluxe room">
                     <ns4:Data Date="2016-07-26" Day="Tuesday">
                        <ns4:Rates>
                           <ns4:Rate Id="35711256365710">
                              <ns4:occupancies>
                                 <ns4:occupancy Code="SG">4800.00</ns4:occupancy>
                                 <ns4:occupancy Code="TW">4800.00</ns4:occupancy>
                                 <ns4:occupancy Code="TR">4800.00</ns4:occupancy>
                              </ns4:occupancies>
                              <ns4:Allotment>2</ns4:Allotment>
                           </ns4:Rate>
                        </ns4:Rates>
                     </ns4:Data>
                     <ns4:Data Date="2016-07-27" Day="Wednesday">
                        <ns4:Rates>
                           <ns4:Rate Id="35711256365710">
                              <ns4:occupancies>
                                 <ns4:occupancy Code="SG">4800.00</ns4:occupancy>
                                 <ns4:occupancy Code="TW">4800.00</ns4:occupancy>
                                 <ns4:occupancy Code="TR">4800.00</ns4:occupancy>
                              </ns4:occupancies>
                              <ns4:Allotment>2</ns4:Allotment>
                           </ns4:Rate>
                        </ns4:Rates>
                     </ns4:Data>
                  </ns4:Room>
                  <ns4:Room Code="2249" CurrencyCode="INR" Name="deluxe room">
                     <ns4:Data Date="2016-07-26" Day="Tuesday">
                        <ns4:Rates>
                           <ns4:Rate Id="98467912814942">
                              <ns4:occupancies>
                                 <ns4:occupancy Code="SG">1400.00</ns4:occupancy>
                                 <ns4:occupancy Code="TW">1400.00</ns4:occupancy>
                                 <ns4:occupancy Code="TR">1400.00</ns4:occupancy>
                              </ns4:occupancies>
                              <ns4:Allotment>0</ns4:Allotment>
                           </ns4:Rate>
                        </ns4:Rates>
                     </ns4:Data>
                     <ns4:Data Date="2016-07-27" Day="Wednesday">
                        <ns4:Rates>
                           <ns4:Rate Id="98467912814942">
                              <ns4:occupancies>
                                 <ns4:occupancy Code="SG">1400.00</ns4:occupancy>
                                 <ns4:occupancy Code="TW">1400.00</ns4:occupancy>
                                 <ns4:occupancy Code="TR">1400.00</ns4:occupancy>
                              </ns4:occupancies>
                              <ns4:Allotment>0</ns4:Allotment>
                           </ns4:Rate>
                        </ns4:Rates>
                     </ns4:Data>
                  </ns4:Room>
                  <ns4:Room Code="3679" CurrencyCode="INR" Name="semi deluxe room">
                     <ns4:Data Date="2016-07-26" Day="Tuesday">
                        <ns4:Rates>
                           <ns4:Rate Id="2798915520018">
                              <ns4:occupancies>
                                 <ns4:occupancy Code="SG">1033.00</ns4:occupancy>
                                 <ns4:occupancy Code="TW">1033.00</ns4:occupancy>
                                 <ns4:occupancy Code="TR">1033.00</ns4:occupancy>
                              </ns4:occupancies>
                              <ns4:Allotment>0</ns4:Allotment>
                           </ns4:Rate>
                        </ns4:Rates>
                     </ns4:Data>
                     <ns4:Data Date="2016-07-27" Day="Wednesday">
                        <ns4:Rates>
                           <ns4:Rate Id="2798915520018">
                              <ns4:occupancies>
                                 <ns4:occupancy Code="SG">1033.00</ns4:occupancy>
                                 <ns4:occupancy Code="TW">1033.00</ns4:occupancy>
                                 <ns4:occupancy Code="TR">1033.00</ns4:occupancy>
                              </ns4:occupancies>
                              <ns4:Allotment>0</ns4:Allotment>
                           </ns4:Rate>
                        </ns4:Rates>
                     </ns4:Data>
                  </ns4:Room>
               </ns4:Rooms>
            </ns4:response>
         </ns4:FetchHotelAllotmentResult>
      </ns4:FetchHotelAllotmentResponse>
   </soap:Body>
</soap:Envelope>"""

    xml_object = remove_namespace_xml(xml)
    xml_response = xml_object.find('response')
    print get_rate_id(xml_response, 1023, '3232')


def remove_namespace_xml(xml_str):
    # instead of ET.fromstring(xml)
    it = iterparse(StringIO(xml_str))
    for _, el in it:
        if '}' in el.tag:
            el.tag = el.tag.split('}', 1)[1]  # strip all namespaces
    root = it.root
    return root

get_data()

