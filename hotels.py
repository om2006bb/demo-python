import json
import requests
import csv
import sys
import codecs
koden=sys.stdin.encoding
import MySQLdb
import csv
from time import gmtime,strftime

db = MySQLdb.connect(host="localhost",  # your host, usually localhost
                     user="root",  # your username
                     passwd="",  # your password
                     db="altacco_backend")  # name of the data base

room_columns = ['HRF_HTLSEQ',
 'HRF_RMTPSEQ',
 'HRF_FCLTCD',
 'HRF_CITYCD',
 'HRF_HTLNAME',
 'HRT_RMTPNAME']

roomtype_columns = ['RMT_RMTPSEQ',
'RMT_NAME',
'RMT_DESC',
'RMT_ACTSTAT',
'RMT_CRUSRID',
'RMT_CRDT',
'RMT_LASTUPDUSRID',
'RMT_LASTUPDDT',
'RMT_GTACD']

hotel_columns = ['HOT_HTLSEQ',
 'HOT_CITYCD',
 'HOT_HTLNAME_backup',
 'HOT_CRUSRID',
 'HOT_CRDT',
 'HOT_STARRT',
 'HOT_ACTSTAT',
 'HOT_WEB',
 'HOT_CHKINTIME',
 'HOT_CHKINAM',
 'HOT_CHKOUTTIME',
 'HOT_CHKOUTAM',
 'HOT_ADDR1',
 'HOT_ADDR2',
 'HOT_ADDR3',
 'HOT_PIN',
 'HOT_CONTACTPERSON',
 'HOT_CONTACTMOBILE',
 'HOT_CONTACTFAX',
 'HOT_CONTACTEMAIL',
 'HOT_PROPTPID',
 'HOT_NAV_FLAG',
 'HOT_CODFLAG',
 'HOT_LAT',
 'HOT_LNG',
 'HOT_CHECK_IN_TYPE',
 'HOT_CURRENCY',
 'HOT_HTLNAME',
 'HOT_DESIGNATION',
 'HOT_PRIORITY',
 'HOT_SAMEDAY_CHECKIN',
 'HOT_APWINDOW',
 'HOT_GDSFLG',
 'HOT_DESC']

def get_checkin_data(identity, db):
    cur = db.cursor()
    if identity == '295':
        return '', '', 'Full_Day'
    cur.execute("Select display_name from base_mastertype where id="+str(identity))
    rows = cur.fetchall()
    split_row = rows[0][0].split(' ')
    if len(split_row)>2:
        if (split_row[0]=='After' or split_row[0]=='Before' )and split_row[2]=='noon':
            time, name, type = split_row[1],'pm','By_Time'
        elif split_row[0] == 'After' or split_row[0]=='Before':
            time, name, type = split_row[1], split_row[2], 'By_Time'
        else:
            time, name, type = split_row[0] + ":" + split_row[1], split_row[2], 'By_Time'
    else:
        time, name, type = split_row[0], split_row[1], 'By_Time'
    return time, name, type

def get_property_type(identity, db):
    cur = db.cursor()
    cur.execute("Select display_name from base_mastertype where id="+str(identity))
    rows = cur.fetchall()
    name= rows[0]
    return name[0]

def get_city_name(region_id,db):
    cur = db.cursor()
    if region_id:
        cur.execute("Select name from regions_region where id ="+str(region_id))
        rows = cur.fetchall()
        return rows[0][0]
    else:
        return ''

def get_city_and_prop_name(property_id, db):
    cur = db.cursor()
    cur.execute("Select name, region_id from properties_property where id =" + str(property_id))
    rows = cur.fetchall()
    region_id = rows[0][1]
    name = rows[0][0]
    if region_id:
        cur.execute("Select name from regions_region where id ="+str(region_id))
        rows = cur.fetchall()
        return name, rows[0][0]
    else:
        return name, ''

def get_city_code(name):
    f = open('city.csv', 'rb')
    reader = csv.reader(f)
    for row in reader:
        if row[1].lower()==name.lower():
            f.close()
            return row[0]
    f.close()
    return ''

def prepare_proptype_dict():
    f = open('proptype.csv', 'rb')
    reader = csv.reader(f)
    dict = {}
    for row in reader:
        dict[row[1].lower().replace(' ','')] = row[0]
    return dict

def get_proptype_id(proptype_dict, name):
    if proptype_dict.get(name.lower().replace(' ','')):
        return proptype_dict.get(name.lower().replace(' ',''))
    else:
        res= [ '', name,'', 1, 'auto-rightstay', strftime('%Y-%m-%d %H:%M:%S', gmtime()),'','','']
        fp = open('proptype_new.csv', 'a')
        writer = csv.writer(fp)
        # writer.writerow(roomtype_columns)
        writer.writerow(res)
    return 2000

def get_city_by_lat_lng(lat, lng):
    latlng_city_url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&sensor=true'
    res = requests.get(latlng_city_url.format(lat, lng))
    res = res.content
    res = json.loads(res)
    res = res['results'][0]['formatted_address']
    res = res.split(',')[-3]
    print "{},{} -> {}".format(lat, lng, res)
    return res

def get_rightstay_properties(db):
    cur = db.cursor()
    fetch_data = "id,name,city_id,description,creation_ts,lastModified,status,emails,contacts,website,address," \
                 "contactperson,pincode,latitude,longitude,listing_type,id_image_url,landmark,checkin_time_display_id,"\
                "checkout_time_display_id,region_id,type_id,is_deleted,address_line_2,area,short_description,priority,"\
                "platform_discount,alternate_contact"
    result = []
    cur.execute("Select "+fetch_data+" from properties_property where id=112 and status=4")
    rows = cur.fetchall()
    proptype_dict = prepare_proptype_dict()
    for row in rows:
        checkin_time, checkin_name,checkin_type = get_checkin_data(row[18],db)
        checkout_time, checkout_name, checkin_type = get_checkin_data(row[19],db)
        property_type = get_property_type(row[21],db)
        property_typeid = get_proptype_id(proptype_dict,property_type)
        city_name = get_city_by_lat_lng(row[13], row[14])
        #city_name = get_city_name(row[20],db)
        city_code = get_city_code(city_name)
        res = (row[0],city_code,row[1],'auto-rightstay',strftime('%Y-%m-%d %H:%M:%S', gmtime()),4,row[6],row[9],checkin_time,checkin_name,checkout_time,checkout_name,row[10],
               row[24],city_name,row[12],row[11],row[8],'',row[7],property_typeid,0,'',row[13],row[14],checkin_type,'INR',row[1],
               'Main Contact','',1,'','',row[3])
        result.append(res)
    fp = open('hotels_data.csv', 'w')
    writer = csv.writer(fp)
    writer.writerow(hotel_columns)
    for res in result:
        writer.writerow(res)
    fp.close()
    print result[:5]

def get_facility_code(facility_dict, mapping_dict, name):
    i, y, l = 1, 1, 1
    # for key in facility_dict.keys():
    #     i += 1
    #     if key == name.lower().replace('_', ' '):
    #         y, k, l = 0, key, 0
    #         return facility_dict[key][0]
    #     if key == name.lower().replace('_', ' ') or name.lower() in key:
    #         if name.lower() == 'heater':
    #             return facility_dict[key][0]
    #         y, k = 0, key
    #     if (i == len(facility_dict) and y == 0):
    #         return facility_dict[key][0]

    # f = open('facility_dump.csv', 'rb')
    # reader = csv.reader(f)
    # for row in reader:
    #     if row[0].lower() == name.lower():
    #         return row[6]
    # f.close()
    val = mapping_dict.get(name.lower())
    if val:
        val = val.split('_')[-1]
    else:
        val = ''
    return val

def prepare_city_dict():
    f = open('city.csv', 'rb')
    reader = csv.reader(f)
    dict = {}
    for row in reader:
        dict[row[-8].lower()] = row[0]
    return dict

def prepare_roomtype_dict():
    f = open('roomtype.csv', 'rb')
    reader = csv.reader(f)
    dict = {}
    for row in reader:
        if not dict.get(row[1].lower().replace(' ','')):
            dict[row[1].lower().replace(' ','')] = row[0]
    return dict

def get_room_type(room_dict, name, room_id):
    if room_dict.get(name.lower().replace(' ','')):
        return name, room_dict.get(name.lower().replace(' ',''))
    else:
        res= [ '', name,'', 'A', 'auto-rightstay', strftime('%Y-%m-%d %H:%M:%S', gmtime()),'','','']
        fp = open('roomtype_new.csv', 'a')
        writer = csv.writer(fp)
        # writer.writerow(roomtype_columns)
        writer.writerow(res)
    return name,room_id

def prepare_facility_data():
    dict,fdict= {},{}
    # fp = open('facility_dump.csv', 'r')
    # rows = csv.reader(fp)
    # for row in rows:
    #     dict[row[4].lower()] = row
    # fp.close()
    fp = open('facility_mapping_dict.csv', 'r')
    rows = csv.reader(fp)
    for row in rows:
        fdict[row[0]] = row[1]
    fp.close()
    return dict,fdict

def get_citycode(dict, name):
    if dict.get(name.lower()):
        return dict.get(name.lower())
    else:
        res = ['', name,'A']
        fp = open('city_new.csv', 'a')
        writer = csv.writer(fp)
        # writer.writerow(roomtype_columns)
        writer.writerow(res)
        dict[name]=''
    return ''


def get_rightstay_properties_rooms(db):
    new_dict = {}
    cur = db.cursor()
    dict = prepare_city_dict()
    room_dict = prepare_roomtype_dict()
    fetch_data = "id,name,property_id,type,host_discount"
    result = []
    cur.execute(
        "select propertyroom_id,(select name from base_mastertype where id = mastertype_id) from properties_propertyroom_amenities")
    rows = cur.fetchall()
    facility_dict,mapping_dict = prepare_facility_data()
    for row in rows:
        facility_cd = get_facility_code(facility_dict, mapping_dict, row[1])
        cur.execute("Select " + fetch_data + " from properties_propertyroom where id ="+str(row[0]))
        rooms = cur.fetchall()
        prop_name, city_name = get_city_and_prop_name(rooms[0][2],db)
        if not new_dict.get(city_name.lower())=='':
            city_code = get_citycode(dict, city_name.lower())
        else:
            city_code = ''
        if city_code=='':
            new_dict[city_name.lower()]=''
        room_type, room_type_seq = get_room_type(room_dict, rooms[0][1], rooms[0][2])
        res = (rooms[0][2], room_type_seq, facility_cd, city_code, prop_name, room_type)
        result.append(res)
    fp = open('rooms_data.csv', 'w')
    writer = csv.writer(fp)
    writer.writerow(room_columns)
    for res in result:
        writer.writerow(res)
    fp.close()
    print result[:5]

def get_rightstay_properties_location(db):
    cur = db.cursor()
    loc_columns = ['LOC_LOCSEQ',
         'LOC_NAME',
         'LOC_CITYID',
         'LOC_CTYCD',
         'LOC_ACTSTAT',
         'LOC_LAT',
         'LOC_LNG',
         'LOC_DESC',
         'LOC_TYPE',
         'LOC_CRUSRID',
         'LOC_CRDT']
    fetch_data = "id,name,landmark,region_id"
    result = []
    cur.execute("Select "+fetch_data+" from properties_property where id=112")
    rows = cur.fetchall()
    for row in rows:
        city_name = get_city_name(row[3], db)
        city_code = get_city_code(city_name)
        res = (row[0],row[2],city_code,'IN','A','','','','Landmarks','auto',)
        result.append(res)
    fp = open('hotel_loc.csv', 'w')
    writer = csv.writer(fp)
    writer.writerow(loc_columns)
    for res in result:
        writer.writerow(res)
    fp.close()
    print result[0:5]


list = get_rightstay_properties(db)
