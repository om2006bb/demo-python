import json
import urllib2
import csv
import sys
import codecs
koden=sys.stdin.encoding
import MySQLdb
import csv
import datetime
from time import gmtime, strftime


fp = open('mmt_facility.csv','w')

writer = csv.writer(fp)
writer.writerow(['FCL_NAME',
 'FCL_DESC',
 'FCL_ACTSTAT',
 'FCL_FCLTTP',
 'FACILITY_GROUP_ID',
 'FCL_FCLTCD'])

def get_propertyimage_data():
    db = MySQLdb.connect(host="localhost",  # your host, usually localhost
                         user="root",  # your username
                         passwd="",  # your password
                         db="altacco_backend")  # name of the data base

    # you must create a Cursor object. It will let
    #  you execute all the queries you need
    cur = db.cursor()

    # Use all the SQL you like

    cur.execute("Select master_type, name, display_name, description, is_enabled, id FROM base_mastertype WHERE master_type in ('PropertyAmenity', 'PropertyRoomAmenity', 'PropertyBathroomAmenity')")

    # print all the first cell of all the rows
    rows = cur.fetchall()

    for row in rows:
        FACILITY_GROUP_ID = 54
        if row[0] == 'PropertyRoomAmenity':
            FACILITY_GROUP_ID = 20
        elif row[0] == 'PropertyBathroomAmenity':
            FACILITY_GROUP_ID = 40
        FCL_FCLTCD = row[5]
        Active_Status = 'IA' if int(row[4]) > 0 else 'A'

        writer.writerow([row[1], row[3], Active_Status, 'ROOM', '', FACILITY_GROUP_ID, FCL_FCLTCD])

    db.close()

    return rows

list = get_propertyimage_data()

for row in list[:5]:
    print row

fp.close()

